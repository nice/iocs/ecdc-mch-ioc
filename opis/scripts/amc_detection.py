# This script is asigned to display widget in carrier window
# Its role is to configure all text fields, LEDs etc. depending on available MTCA modules
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model.properties import WidgetColor
from org.csstudio.display.builder.model.properties import ActionInfos
from org.csstudio.display.builder.model.properties import OpenDisplayActionInfo
from org.csstudio.display.builder.model.properties.OpenDisplayActionInfo import Target
from org.csstudio.display.builder.model.widgets import ActionButtonWidget
from java.util import Arrays

# To write a portable script, check for the display builder's widget type:
display_builder = 'getVersion' in dir(widget)

if display_builder:
    phoebus = 'PHOEBUS' in dir(ScriptUtil)
    if phoebus:
		from org.phoebus.framework.macros import Macros
    else:
		from org.csstudio.display.builder.model.macros import Macros
else:
    from org.csstudio.opibuilder.scriptUtil import PVUtil, ConsoleUtil


# function to get reference to display screen
def getDisplay():
	display = widget.getDisplayModel()
	return display

# function to get module's FruId
def getFruId(pv_name):
	try:
		# create listener on PV
		pv = PVUtil.createPV(pv_name, 2000)
		# read PV
		id = pv.read().getValue()
		# release listener
		PVUtil.releasePV(pv)
	except:
		ScriptUtil.getLogger().severe("Can't read FruId from " + pv_name)
		id = 0
	return id


# set fields assigned to AMCs
def setAMCs():
	macros = display.getEffectiveMacros()
	name_mode = macros.getValue('NAME_MODE')
	# up to 12 amcs in any crate
	if name_mode == '0':
		for i in range (1, 13):
			macro_name = 'SLOT' + str(i) + '_MODULE'
			macro_idx = 'SLOT' + str(i) + '_IDX'
			macros = display.getEffectiveMacros()
			pmacro = macros.getValue('P')
			module_macro = macros.getValue(macro_name)
			idx_macro = macros.getValue(macro_idx)
			crate_macro = macros.getValue('CRATE_NUM')
			# suffixes for pv_names and update fields names
			suffixes = ['FruName', 'Slot', 'FruId']
			# check if macro is defined
			if len(module_macro) > 0:
				# find state LED and set its pv_name and state0 color
				led = ScriptUtil.findWidgetByName(display, 'AMC' + str(i) + '_MultiLED')
				led.setPropertyValue('pv_name', pmacro + '-' + module_macro + '-' + crate_macro + idx_macro + ':State')
				led.setPropertyValue("states[0].color", red)
				# find sensor button
				sensor_button = ScriptUtil.findWidgetByName(display, 'AMC' + str(i) + '_Sensor_button')
				# generate macros to be passed to sensor window
				sensor_macros = Macros()
				sensor_macros.add('MODULE', '-' + module_macro + '-')
				sensor_macros.add('IDX', idx_macro + ':')
				#sensor_macros.add('CRATE_NUM', '-' + crate_macro)
				# Get FruId
				fruId = getFruId(pmacro + '-' + module_macro + '-' + crate_macro + idx_macro + ':FruId')
				# generate macros for expert window
				expert_macros = Macros()
				expert_macros.add('MODULE', module_macro)
				expert_macros.add('IDX', idx_macro + ':')
				expert_macros.add('FRUID', str(fruId))
				# generate macros for rtm window
				rtm_macros = Macros()
				rtm_macros.add('MODULE', 'RTM')
				rtm_macros.add('IDX', idx_macro + ':')
				# Prepare action for sensor button
				open_info = OpenDisplayActionInfo("Details", "../sensors/Sensors.bob", sensor_macros, Target.WINDOW)
				info_list = Arrays.asList(open_info)
				actions_info = ActionInfos(info_list)
				sensor_button.propActions().setValue(actions_info)
				sensor_button.setPropertyValue('enabled', True)
				# find more button
				more_button = ScriptUtil.findWidgetByName(display, 'AMC' + str(i) + '_More_button')
				more_button.setPropertyValue('enabled', True)
				# prepare actions for more button
				# get already assigned links action
				links = more_button.propActions().getValue()
				actions = links.getActions()
				# generate three actions: open rtm window, fru details window and expert window
				open_info1 = OpenDisplayActionInfo("RTM", "../rtm/RTM.bob", rtm_macros, Target.WINDOW)
				open_info2 = OpenDisplayActionInfo("FRU", "../fru/FRU.bob", sensor_macros, Target.WINDOW)
				open_info3 = OpenDisplayActionInfo("Expert", "../expert/ExpertFRU.bob", expert_macros, Target.WINDOW)
				info_list = Arrays.asList(open_info1, open_info2, actions[0], open_info3)
				links_macro = actions[0].getMacros()
				links_macro.add('MODULE', module_macro)
				links_macro.add('IDX', idx_macro + ':')
				actions_info = ActionInfos(info_list)
				more_button.propActions().setValue(actions_info)
				# find update fields with FruName, Slot, FruId
				for j in suffixes:
					update = ScriptUtil.findWidgetByName(display, 'AMC' + str(i) + '_update_' + j)
					update.setPropertyValue('pv_name', pmacro + '-' + module_macro + '-' + crate_macro + idx_macro + ':' + j)
					update.setPropertyValue('background_color', enable)
				# find rtm LED
				rtm = ScriptUtil.findWidgetByName(display, 'RTM' + str(i) + '_LED')
				rtm_macro = macros.getValue('SLOT' + str(i+16) + '_MODULE')
				rtm.setPropertyValue('off_color', green)
				# check if rtm macro is defined
				if len(rtm_macro) > 0:
					rtm.setPropertyValue('pv_name', pmacro + '-' + rtm_macro + '-' + crate_macro + idx_macro + ':P')
	else:
		ScriptUtil.getLogger().severe("name_mode=1")
		pmacro = macros.getValue('P')
		suffixes = ['FruName', 'Slot', 'FruId']
		for i in range (1, 13):
			macro_name = 'SLOT' + str(i) + '_MODULE'
			macro_idx = 'SLOT' + str(i) + '_IDX'
			module_macro = macros.getValue(macro_name)
			idx_macro = macros.getValue(macro_idx)
			if len(module_macro)>0:
				led = ScriptUtil.findWidgetByName(display, 'AMC' + str(i) + '_MultiLED')
				led.setPropertyValue('pv_name', pmacro + 'Ctrl-' + module_macro + '-' + idx_macro + ':State')
				# find sensor button
				sensor_button = ScriptUtil.findWidgetByName(display, 'AMC' + str(i) + '_Sensor_button')
				# generate macros to be passed to sensor window
				sensor_macros = Macros()
				sensor_macros.add('P', pmacro + 'Ctrl-')
				sensor_macros.add('CRATE_NUM', '-')
				sensor_macros.add('MODULE', module_macro)
				sensor_macros.add('IDX', idx_macro + ':')
				# Prepare action for sensor button
				open_info = OpenDisplayActionInfo("Details", "../sensors/Sensors.bob", sensor_macros, Target.WINDOW)
				info_list = Arrays.asList(open_info)
				actions_info = ActionInfos(info_list)
				sensor_button.propActions().setValue(actions_info)
				sensor_button.setPropertyValue('enabled', True)
				# fru macros
				fru_macros = Macros()
				fru_macros.add('P', pmacro + 'Ctrl-')
				fru_macros.add('CRATE_NUM', '-')
				fru_macros.add('MODULE', module_macro)
				fru_macros.add('IDX', idx_macro + ':')
				open_info2 = OpenDisplayActionInfo("FRU", "../fru/FRU.bob", fru_macros, Target.WINDOW)
				# generate macros for rtm window
				rtm_macros = Macros()
				rtm_macros.add('P', pmacro + 'Ctrl')
				rtm_macros.add('CRATE_NUM', '')
				rtm_module = macros.getValue('SLOT' + str(i+16) + '_MODULE')
				rtm_idx = macros.getValue('SLOT' + str(i+16) + '_IDX')
				rtm_macros.add('IDX', str(rtm_idx))
				rtm_macros.add('MODULE', str(rtm_module))
				open_info1 = OpenDisplayActionInfo("RTM", "../rtm/RTM.bob", rtm_macros, Target.WINDOW)
				# find more button
				more_button = ScriptUtil.findWidgetByName(display, 'AMC' + str(i) + '_More_button')
				more_button.setPropertyValue('enabled', True)
				# Get FruId
				fruId = getFruId(pmacro + 'Ctrl-' + module_macro + '-' + idx_macro + ':FruId')
				# generate macros for expert window
				expert_macros = Macros()
				expert_macros.add('P', pmacro + 'Ctrl')
				expert_macros.add('MODULE', module_macro)
				expert_macros.add('CRATE_NUM', '')
				expert_macros.add('IDX', idx_macro + ':')
				expert_macros.add('FRUID', str(fruId))
				open_info3 = OpenDisplayActionInfo("Expert", "../expert/ExpertFRU.bob", expert_macros, Target.WINDOW)
				# get already assigned links action
				links = more_button.propActions().getValue()
				actions = links.getActions()
				links_macro = actions[0].getMacros()
				links_macro.add('MODULE', module_macro)
				links_macro.add('IDX', idx_macro + ':')
				links_macro.add('CRATE_NUM', '')
				links_macro.add('P', pmacro + 'Ctrl')
				# add actions for more_button
				info_list = Arrays.asList(open_info1, open_info2, actions[0], open_info3)
				actions_info = ActionInfos(info_list)
				more_button.propActions().setValue(actions_info)
				# activate widgets
				for j in suffixes:
					update = ScriptUtil.findWidgetByName(display, 'AMC' + str(i) + '_update_' + j)
					update.setPropertyValue('pv_name', pmacro + 'Ctrl-' + module_macro + '-' + idx_macro + ':' + j)
					update.setPropertyValue('background_color', enable)
				# find rtm LED
				rtm = ScriptUtil.findWidgetByName(display, 'RTM' + str(i) + '_LED')
				rtm_macro = macros.getValue('SLOT' + str(i+16) + '_MODULE')
				rtm.setPropertyValue('off_color', green)
				# check if rtm macro is defined
				if len(rtm_macro) > 0:
					rtm.setPropertyValue('pv_name', pmacro + 'Ctrl-' + module_macro + '-' + idx_macro + ':P')


# set all fields assigned to additionals units
def setAdditionals(pmacro, crate_macro, idx, module_name, down, up):
	macros = display.getEffectiveMacros()
	name_mode = macros.getValue('NAME_MODE')
	# up to 12 amcs in any crate
	if name_mode == '0':
		# in range of maximum possible units in any crate
		for i in range(1, idx + 1):
			if module_name in ['Clk', 'PCI']:
				pv_name =  pmacro + '-MTCA-' + crate_macro + '00:' + module_name + '-'
			else:
				pv_name =  pmacro + '-' + module_name + '-' + crate_macro + '0' + str(i) + ':'
			presence = pv_name + 'P'
			try:
				# create PV instance
				pv = PVUtil.createPV(presence, 2000)
				# read PV with state info
				state = pv.read().getValue()
				# release listener
				PVUtil.releasePV(pv)
				# if PV is available and its value is 1 (presence = 1)
				if state:
					for j in ['FruName', 'FruId']:
						# set update fields
						unit = ScriptUtil.findWidgetByName(display, module_name + str(i) + '_update_' + j)
						unit.setPropertyValue('pv_name', pv_name + j)
						unit.setPropertyValue('background_color', enable)
					# set sensor button
					sensor_button = ScriptUtil.findWidgetByName(display, module_name + str(i) + '_More_button' )
					sensor_button.setPropertyValue('enabled', True)
					# set fru details button
					fru_button = ScriptUtil.findWidgetByName(display, module_name + str(i) + '_Sensor_button')
					fru_button.setPropertyValue('enabled', True)
					# set state LED
					led = ScriptUtil.findWidgetByName(display, module_name + str(i) + '_MultiLED')
					led.setPropertyValue("states[0].color", red)
					led.setPropertyValue('pv_name', pv_name + 'State')
			except:
				break
	else:
		for i in range(down, up+1):
			if module_name in ['Clk', 'PCI']:
				pv_name =  pmacro + ':' + module_name + '-'
			else:
				module_macro = macros.getValue('SLOT' + str(i) + '_MODULE')
				idx_macro = macros.getValue('SLOT' + str(i) + '_IDX')
				pv_name =  pmacro + 'Ctrl-' + str(module_macro) + '-' + str(idx_macro) + ':'
			presence = pv_name + 'P'
			try:
				# create PV instance
				pv = PVUtil.createPV(presence, 2000)
				# read PV with state info
				state = pv.read().getValue()
				# release listener
				PVUtil.releasePV(pv)
				# if PV is available and its value is 1 (presence = 1)
				if state:
					for j in ['FruName', 'FruId']:
						# set update fields
						unit = ScriptUtil.findWidgetByName(display, module_name + str(i-down+1) + '_update_' + j)
						unit.setPropertyValue('pv_name', pv_name + j)
						unit.setPropertyValue('background_color', enable)
					# set sensor button
					fru_button = ScriptUtil.findWidgetByName(display, module_name + str(i-down+1) + '_More_button' )
					fru_button.setPropertyValue('enabled', True)
					# set fru details button
					sensor_button = ScriptUtil.findWidgetByName(display, module_name + str(i-down+1) + '_Sensor_button')
					sensor_button.setPropertyValue('enabled', True)
					sensor_macro = Macros()
					#fru macros
					fru_macros = Macros()
					fru_macros.add('CRATE_NUM', '')
					# adapt sensor and fru macros depending on module
					if module_name in ['Clk', 'PCI']:
						sensor_macro.add('IDX', 'Clk-')
						sensor_macro.add('MODULE', ':')
						sensor_macro.add('CRATE_NUM', '')
						fru_macros.add('MODULE', ':')
						fru_macros.add('IDX', 'Clk-')
					else:
						sensor_macro.add('IDX', str(idx_macro) + ':')
						sensor_macro.add('MODULE', str(module_macro))
						sensor_macro.add('P', pmacro + 'Ctrl-')
						sensor_macro.add('CRATE_NUM', '-')
						fru_macros.add('CRATE_NUM', '-')
						fru_macros.add('MODULE', str(module_macro))
						fru_macros.add('P', pmacro + 'Ctrl-')
						fru_macros.add('IDX', str(idx_macro) + ':')
					# action for sensor button
					open_info = OpenDisplayActionInfo("Sensors", "../sensors/Sensors.bob", sensor_macro, Target.WINDOW)
					info_list = Arrays.asList(open_info)
					actions_info = ActionInfos(info_list)
					sensor_button.propActions().setValue(actions_info)
					# action for fru button
					open_info2 = OpenDisplayActionInfo("FRU", "../fru/FRU.bob", fru_macros, Target.WINDOW)
					info_list = Arrays.asList(open_info2)
					actions_info = ActionInfos(info_list)
					fru_button.propActions().setValue(actions_info)
					# set state LED
					led = ScriptUtil.findWidgetByName(display, module_name + str(i-down+1) + '_MultiLED')
					led.setPropertyValue("states[0].color", red)
					led.setPropertyValue('pv_name', pv_name + 'State')
			except:
				continue


# set additionales modules like PM, CU, Clock, PCI
def setRest():
	macros = display.getEffectiveMacros()
	pmacro = macros.getValue('P')
	crate_macro = macros.getValue('CRATE_NUM')
	setAdditionals(pmacro, crate_macro, 4, 'PM', 31, 34)
	setAdditionals(pmacro, crate_macro, 2, 'CU', 48, 49)
	setAdditionals(pmacro, crate_macro, 1, 'Clk', 64, 64)
	setAdditionals(pmacro, crate_macro, 1, 'PCI', 65, 65)


if __name__ == "__main__":
	# Get display reference
	display = getDisplay()
	# colors definitions
	enable = WidgetColor(230, 235, 232)
	red = WidgetColor(252, 13, 27)
	green = WidgetColor(90, 110, 90)
	# set amcs and rtms fields
	setAMCs()
	# set additional modules (PM, CU, Clock, PCI)
	setRest()
