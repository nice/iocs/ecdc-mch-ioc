# This script is to inform user about the current naming convention of the panels
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
# To write a portable script, check for the display builder's widget type:
display_builder = 'getVersion' in dir(widget)

if display_builder:
    phoebus = 'PHOEBUS' in dir(ScriptUtil)
    if phoebus:
		from org.phoebus.framework.macros import Macros
    else:
		from org.csstudio.display.builder.model.macros import Macros
else:
    from org.csstudio.opibuilder.scriptUtil import PVUtil, ConsoleUtil
    ConsoleUtil.writeInfo("Executing in BOY")


# get display reference
def getDisplay():
	display = widget.getDisplayModel()
	return display


if __name__ == "__main__":
	# get display reference
	display = getDisplay()
	# get macros to generate PVs names
	macro = display.getEffectiveMacros()
	# get macro responsible for naming style
	name_mode = macro.getValue('NAME_MODE')
	# find widget
	field = ScriptUtil.findWidgetByName(display, 'Naming_label')
	if name_mode == '0':
		field.setPropertyValue('text', 'Standard')
	else:
		field.setPropertyValue('text', 'Custom')
