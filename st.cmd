require ipmimanager

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","10000000")

epicsEnvSet("P", "ECDC-B02:Ctrl")

# when set to 0 console will not produce message after ioc init, when set to 1 all messages will be visible
epicsEnvSet("DISP_MSG", 0)

# crate IP address
epicsEnvSet("CRATE_NUM", "1")
epicsEnvSet("MCH_ADDR", "172.30.38.53")
epicsEnvSet("TELNET_PORT", "23")

# 1=enable/0=disable INTERNAL archiver
epicsEnvSet("ARCHIVER", "0")
# INTERNAL archiver size
epicsEnvSet("ARCHIVER_SIZE", 1024)

# Panels compatibility version
epicsEnvSet("PANEL_VER", "2.1.0")

# Special naming mode 0 -> standard naming convention 1 -> special mode
epicsEnvSet("NAME_MODE", 0)

# Deployment path (with substitutions files)
epicsEnvSet("DEPLOYMENT_DIR", "$(E3_CMD_TOP)")

###########################
# connect to specific MCH
############################
epicsEnvSet("MTCA_PREF", "$(P)-MTCA-$(CRATE_NUM)00:")
epicsEnvSet("IOC_PREF", "$(P)-IOC-$(CRATE_NUM)00:")

epicsEnvSet("SLOT2_MODULE", "CPU")
epicsEnvSet("SLOT2_IDX", "01")
epicsEnvSet("SLOT3_MODULE", "EVM")
epicsEnvSet("SLOT3_IDX", "01")
epicsEnvSet("SLOT5_MODULE", "EVM")
epicsEnvSet("SLOT5_IDX", "02")
epicsEnvSet("SLOT6_MODULE", "EVM")
epicsEnvSet("SLOT6_IDX", "03")

epicsEnvSet("CHASSIS_CONFIG","SLOT2_MODULE=$(SLOT2_MODULE)", "SLOT2_IDX=$(SLOT2_IDX)", "SLOT3_MODULE=$(SLOT3_MODULE)","SLOT3_IDX=$(SLOT3_IDX)","SLOT5_MODULE=$(SLOT5_MODULE)", "SLOT5_IDX=$(SLOT5_IDX)", "SLOT6_MODULE=$(SLOT6_MODULE)", "SLOT6_IDX=$(SLOT6_IDX)")

# load db templates for your hardware configuration
iocshLoad("chassis.iocsh")

iocshLoad("$(ipmimanager_DIR)connect.iocsh", "DB_NAME=dynamic, MCH_ADDR=$(MCH_ADDR), ARCHIVER=$(ARCHIVER), ARCHIVER_SIZE=$(ARCHIVER_SIZE), P=$(P), CRATE_NUM=$(CRATE_NUM), TIMEOUT=10, USE_STREAM=, STREAM_PORT=$(TELNET_PORT),CHECK_VER=#,PANEL_VER=$(PANEL_VER),NAME_MODE=$(NAME_MODE),MTCA_PREF=$(MTCA_PREF),IOC_PREF=$(IOC_PREF),USE_EXPERT=,$(CHASSIS_CONFIG),DEPLOYMENT_DIR=$(DEPLOYMENT_DIR)")  # With steam reading links (mch not password protected)

iocInit()

eltc "$(DISP_MSG)"
